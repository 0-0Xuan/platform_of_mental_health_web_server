package com.qg.xuan.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qg.xuan.bean.Grade;
import com.qg.xuan.bean.GsonBean;
import com.qg.xuan.bean.Option;
import com.qg.xuan.bean.Paper;
import com.qg.xuan.bean.Question;
import com.qg.xuan.bean.Situation;
import com.qg.xuan.bean.User;
import com.qg.xuan.service.PaperService;
import com.qg.xuan.util.ControllerUtil;
import com.qg.xuan.util.FinalValue;
import com.qg.xuan.util.ShareUtil;

@Controller
public class PaperController {

    PaperService paperService;

    public void setPaperService(PaperService paperService) {
	this.paperService = paperService;
    }

    @ResponseBody
    @RequestMapping(value = "/createPaper", method = RequestMethod.POST)
    public String createPaper(HttpServletRequest request) {
	Paper paper = ShareUtil.gson.fromJson(request.getParameter("param"),
		Paper.class);
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.WORKER) {
	    int state = paperService.createPaper(paper, user.userId);
	    if (state == FinalValue.SUCCESS) {
		return ControllerUtil.successBean;
	    } else {
		ControllerUtil.gsonBean.state = FinalValue.FAIL;
		return ControllerUtil.getGsonBeanString();
	    }
	}
	return null;

    }

    @ResponseBody
    @RequestMapping(value = "/getStudentNotWritePaper", method = RequestMethod.POST)
    public String getNotStudentWritePaper(HttpServletRequest request) {
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.STUDENT) {
	    String bean = ShareUtil.gson.toJson(paperService
		    .getStudentNotWritePaper(user.userId));
	    return ShareUtil.gson
		    .toJson(new GsonBean(FinalValue.SUCCESS, bean));
	}
	return null;

    }

    @ResponseBody
    @RequestMapping(value = "/writePaper", method = RequestMethod.POST)
    public String writePaper(HttpServletRequest request) {
	Paper paper = ShareUtil.gson.fromJson(request.getParameter("param"),
		Paper.class);
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.STUDENT) {
	    System.out.println("paperService.writePaper");
	    int state = paperService.writePaper(paper, user.userId);
	    if (state == FinalValue.SUCCESS) {
		return ControllerUtil.successBean;
	    } else {
		ControllerUtil.gsonBean.state = FinalValue.FAIL;
		return ControllerUtil.getGsonBeanString();
	    }
	}
	return null;

    }

    @ResponseBody
    @RequestMapping(value = "/getGrade", method = RequestMethod.POST)
    public String getGrade(HttpServletRequest request) {
	String userId = request.getParameter("param");
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.WORKER) {
	    System.out.println("getGrade获取一份数据");
	    Grade grade = paperService.getGrade(userId);
	    ControllerUtil.gsonBean.bean=ShareUtil.gson.toJson(grade);
	    ControllerUtil.gsonBean.state = FinalValue.SUCCESS;
	    return ControllerUtil.getGsonBeanString();
	}
	return null;

    }
    
    @ResponseBody
    @RequestMapping(value = "/getGrades", method = RequestMethod.POST)
    public String getGrades(HttpServletRequest request) {
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.WORKER) {
	    System.out.println("getGrades");
	    List<Grade> grades = paperService.getGrades(user.userId);
	    ControllerUtil.gsonBean.bean=ShareUtil.gson.toJson(grades);
	    ControllerUtil.gsonBean.state = FinalValue.SUCCESS;
	    return ControllerUtil.getGsonBeanString();
	}
	return null;

    }
    
    @ResponseBody
    @RequestMapping(value = "/getSituation", method = RequestMethod.POST)
    public String getSituation(HttpServletRequest request) {
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.WORKER) {
	    System.out.println("getGrades");
	    List<Situation> situations = paperService.getSituations(user.userId);
	    ControllerUtil.gsonBean.bean=ShareUtil.gson.toJson(situations);
	    ControllerUtil.gsonBean.state = FinalValue.SUCCESS;
	    return ControllerUtil.getGsonBeanString();
	}
	return null;

    }

    @ResponseBody
    @RequestMapping(value = "/testPaper", method = RequestMethod.GET)
    public String testPaper(HttpServletRequest request) {
	Paper paper = new Paper();
	paper.instructions = "心理问卷  广东工业大学--607版";
	paper.questions = new ArrayList<Question>();
	for (int i = 0; i < 5; i++) {
	    Question q = new Question();
	    q.options = new ArrayList<Option>();
	    q.content = "问题" + i;
	    for (int k = 0; k < 5; k++) {
		Option o = new Option();
		o.content = "选项" + k;
		o.grade = k;
		q.options.add(o);
	    }
	    paper.questions.add(q);
	}
	User user = ControllerUtil.getRequestUser(request);
	// System.out.println(ShareUtil.gson.toJson(user));
	// ShareUtil.print(paper);
	if (user != null && user.role == FinalValue.WORKER) {
	    int state = paperService.createPaper(paper, user.userId);
	    if (state == FinalValue.SUCCESS) {
		return ControllerUtil.successBean;
	    } else {
		ControllerUtil.gsonBean.state = FinalValue.FAIL;
		return ControllerUtil.getGsonBeanString();
	    }
	}
	return null;

    }

}

package com.qg.xuan.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.reflect.TypeToken;
import com.qg.xuan.bean.GsonBean;
import com.qg.xuan.bean.Student;
import com.qg.xuan.bean.User;
import com.qg.xuan.bean.Worker;
import com.qg.xuan.service.UserService;
import com.qg.xuan.util.ControllerUtil;
import com.qg.xuan.util.FinalValue;
import com.qg.xuan.util.ShareUtil;

@Controller
public class UserController {

    UserService userService;

    public void setUserService(UserService userService) {
	this.userService = userService;
    }

    @ResponseBody
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(HttpServletRequest request) {
	User user = ShareUtil.gson.fromJson(request.getParameter("param"),
		User.class);
	String password = request.getParameter("password");
	System.out.println("用户"+ShareUtil.gson.toJson(user) + "密码" +password);
	return userService.register(user, password);
    }

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(HttpServletRequest request) {
	User user = ShareUtil.gson.fromJson(request.getParameter("param"),
		User.class);
	String password = request.getParameter("password");
	GsonBean bean = new GsonBean();
	if (userService.login(user, password) == true) {

//	    request.getSession().setAttribute("pmh_xuan",
//		    ShareUtil.gson.toJson(user));
	    bean.state = FinalValue.SUCCESS;
	    return ShareUtil.gson.toJson(bean);

	} else {
	    bean.state = FinalValue.FAIL;
	    return ShareUtil.gson.toJson(bean);
	}
    }

    @ResponseBody
    @RequestMapping(value = "/exit", method = RequestMethod.POST)
    public String exit(HttpServletRequest request) {
	request.getSession().setAttribute("pmh_xuan", null);
	return null;
    }

    @ResponseBody
    @RequestMapping(value = "/sureUser", method = RequestMethod.POST)
    public String sureUser(HttpServletRequest request) {
	String userId = request.getParameter("param");
	User user = ControllerUtil.getRequestUser(request);
	if (user != null) {
	    User newUser = new User();
	    newUser.userId = userId;
	    switch (user.role) {
	    case FinalValue.WORKER:
		newUser.role = FinalValue.STUDENT;
		break;
	    case FinalValue.MANAGER:
		newUser.role = FinalValue.WORKER;
		break;
	    }
	    if (userService.sureUser(newUser)) {
		return ControllerUtil.successBean;
	    }
	}
	ControllerUtil.gsonBean.state = FinalValue.FAIL;
	return ControllerUtil.getGsonBeanString();
    }

    @ResponseBody
    @RequestMapping(value = "/refuseUser", method = RequestMethod.POST)
    public String refuseUser(HttpServletRequest request) {
	String userId = request.getParameter("param");
	User user = ControllerUtil.getRequestUser(request);
	if (user != null) {
	    User newUser = new User();
	    newUser.userId = userId;
	    switch (user.role) {
	    case FinalValue.WORKER:
		newUser.role = FinalValue.STUDENT;
		break;
	    case FinalValue.MANAGER:
		newUser.role = FinalValue.WORKER;
		break;
	    }
	    if (userService.refuseUser(newUser)) {
		return ControllerUtil.successBean;
	    }
	}
	ControllerUtil.gsonBean.state = FinalValue.FAIL;
	return ControllerUtil.getGsonBeanString();
    }

    @ResponseBody
    @RequestMapping(value = "/getUser", method = RequestMethod.POST)
    public String getUser(HttpServletRequest request) {
	String userId = request.getParameter("param");
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.WORKER) {
	    User fillUser = new User();
	    fillUser.userId = userId;
	    boolean result = userService.fillUserInfo(fillUser);
	    if (result) {
		ControllerUtil.gsonBean.state = FinalValue.SUCCESS;
		ControllerUtil.gsonBean.bean = ShareUtil.gson.toJson(user);
		return ControllerUtil.getGsonBeanString();
	    }
	}
	ControllerUtil.gsonBean.state = FinalValue.FAIL;
	return ControllerUtil.getGsonBeanString();
    }

    @ResponseBody
    @RequestMapping(value = "/getManageStudents", method = RequestMethod.POST)
    public String getManageStudents(HttpServletRequest request) {
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.WORKER) {
	    List<Student> students = userService.getManageStudents(user.userId);
	    ControllerUtil.gsonBean.state = FinalValue.SUCCESS;
	    ControllerUtil.gsonBean.bean = ShareUtil.gson.toJson(students);
	    return ControllerUtil.getGsonBeanString();
	}
	ControllerUtil.gsonBean.state = FinalValue.FAIL;
	return ControllerUtil.getGsonBeanString();
    }

    @ResponseBody
    @RequestMapping(value = "/getNotSureWorker", method = RequestMethod.POST)
    public String getNotSureWorker(HttpServletRequest request) {
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && user.role == FinalValue.MANAGER) {
	    ControllerUtil.gsonBean.state = FinalValue.SUCCESS;
	    ControllerUtil.gsonBean.bean = ShareUtil.gson.toJson(userService
		    .getNotSureWorker());
	    return ControllerUtil.getGsonBeanString();
	}
	ControllerUtil.gsonBean.state = FinalValue.FAIL;
	return ControllerUtil.getGsonBeanString();
    }

    @ResponseBody
    @RequestMapping(value = "/managerUser", method = RequestMethod.POST)
    public String managerUser(HttpServletRequest request) {
	String userId = request.getParameter("param");
	User user = ControllerUtil.getRequestUser(request);
	if (user != null) {
	    boolean result = userService.manageUser(user, userId);
	    if (result) {
		return ControllerUtil.successBean;
	    }
	}
	ControllerUtil.gsonBean.state = FinalValue.FAIL;
	return ControllerUtil.getGsonBeanString();
    }

    @ResponseBody
    @RequestMapping(value = "/managerUsers", method = RequestMethod.POST)
    public String managerUsers(HttpServletRequest request) {
	String json = request.getParameter("param");
	User user = ControllerUtil.getRequestUser(request);
	if (user != null && json != null) {
	    List<String> users = ShareUtil.gson.fromJson(json,
		    new TypeToken<List<String>>() {
		    }.getType());
	    boolean result = userService.manageUsers(user, users);
	    if (result) {
		return ControllerUtil.successBean;
	    }
	}
	ControllerUtil.gsonBean.state = FinalValue.FAIL;
	return ControllerUtil.getGsonBeanString();
    }

    @ResponseBody
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(HttpServletRequest request) {
	Worker user = new Worker();
	user.birthday = "1993-1-1";
	user.name = "xx";
	user.role = 2;
	user.sex = "m";
	user.userId = "1";
	user.phone = "13631462543";
	String password = "1";
	System.out.println("test");
	return userService.register(user, password);
    }

    @ResponseBody
    @RequestMapping(value = "/loginTest", method = RequestMethod.GET)
    public String loginTest(HttpServletRequest request) {
	Worker user = new Worker();
	user.birthday = "1993-1-1";
	user.name = "xx";
	user.role = 2;
	user.sex = "m";
	user.userId = "1";
	user.phone = "13631462543";
	String password = "1";
	GsonBean bean = new GsonBean();
	if (userService.login(user, password) == true) {

	    request.getSession().setAttribute("pmh_xuan",
		    ShareUtil.gson.toJson(user));
	    bean.state = FinalValue.SUCCESS;
	    return ShareUtil.gson.toJson(bean);

	} else {
	    bean.state = FinalValue.FAIL;
	    return ShareUtil.gson.toJson(bean);
	}
    }
}

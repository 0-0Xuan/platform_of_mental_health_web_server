package com.qg.xuan.util;

public class FinalValue {
    public static final int STUDENT = 1;
    public static final int WORKER = 2;
    public static final int MANAGER = 3;
    
    //角色状态
    public static final int DEFAULT = 0;
    public static final int ACTIVE = 1;
    public static final int REFUSE = 2;
    
    
    public static int SUCCESS=0;  //操作成功
    public static int FAIL=1;	//操作失败,无错误信息
}

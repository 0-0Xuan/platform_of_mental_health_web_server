package com.qg.xuan.util;

import javax.servlet.http.HttpServletRequest;

import com.qg.xuan.bean.GsonBean;
import com.qg.xuan.bean.User;
import com.qg.xuan.service.PaperService;

public class ControllerUtil {

    public static GsonBean gsonBean = new GsonBean();
    public static String successBean = ShareUtil.gson.toJson(gsonBean); 

    public static User getRequestUser(HttpServletRequest request) {
	String json = request.getParameter("pmh_xuan");
	if(json!=null){
	    User user = ShareUtil.gson.fromJson(json, User.class);
	    return user;
	}else{
	    System.out.println("user为空");
	    return null;
	}
    }
    
    public static String getGsonBeanString(){
	return ShareUtil.gson.toJson(gsonBean);
    }
}

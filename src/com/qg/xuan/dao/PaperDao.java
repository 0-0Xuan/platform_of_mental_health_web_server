package com.qg.xuan.dao;

import java.util.List;

import com.qg.xuan.bean.Grade;
import com.qg.xuan.bean.Option;
import com.qg.xuan.bean.Paper;
import com.qg.xuan.bean.Question;

public interface PaperDao {
    public int createPaper(Paper paper, String workNum);

    public Paper getPaper(String paperNum);
    
    public int createQuestion(Question question, String paperNum);

    public int createOption(Option option, String questionNum);

    public List<Paper> getStudentPaperGrade(String studentNum);

    public List<Paper> getStudentNotWritePaper(String studentNum);

    public List<Paper> getPapers(String workNum);

    public List<Option> getOption(String questionNum);

    public List<Question> getQuestion(String paperNum);
    
    public int writePaper(Paper paper, String studentNum);
}

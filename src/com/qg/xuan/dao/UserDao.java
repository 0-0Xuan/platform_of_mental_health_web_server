package com.qg.xuan.dao;

import java.util.List;

import com.qg.xuan.bean.Student;
import com.qg.xuan.bean.User;
import com.qg.xuan.bean.Worker;

public interface UserDao {
    public boolean hasUser(User user, String password);

    public boolean insertUser(User user, String password);

    public List<String> getWorkNum(String studentNum);

    public boolean sureUser(User user, int state);

    public boolean manageUser(User user, String userId);

    public boolean getUser(User user);

    public List<Worker> getNotSureWorker();

    public List<Student> getManageStudents(String workerId);
}

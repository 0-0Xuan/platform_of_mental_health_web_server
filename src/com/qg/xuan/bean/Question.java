package com.qg.xuan.bean;

import java.util.List;

public class Question {
    public int questionNum;
    public String content;
    public List<Option> options;
}

package com.qg.xuan.bean;

import com.qg.xuan.util.FinalValue;

public class GsonBean {
    public int state = FinalValue.SUCCESS; // 操作的结果
    public String bean; // 操作返回的数据

    public GsonBean() {
	// TODO Auto-generated constructor stub

    }

    public GsonBean(int state) {
	// TODO Auto-generated constructor stub
	this.state = state;
    }

    public GsonBean(int state, String bean) {
	this.state = state;
	this.bean = bean;
    }

}

package com.qg.xuan.bean;

import java.util.List;

public class Paper {
    public int paperNum;
    public String instructions;
    public int grade;
    public List<Question> questions;
}

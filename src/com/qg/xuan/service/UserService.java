package com.qg.xuan.service;

import java.util.List;

import com.qg.xuan.bean.Student;
import com.qg.xuan.bean.User;
import com.qg.xuan.bean.Worker;

public interface UserService {
    public String register(User user,String password);
    public boolean login(User user,String password);
    public boolean sureUser(User user);
    public boolean refuseUser(User user);
    public boolean manageUser(User user,String userId);
    public boolean manageUsers(User user,List<String> users);
    public boolean fillUserInfo(User user);
    public List<Worker> getNotSureWorker();
    public List<Student> getManageStudents(String workerId);
}

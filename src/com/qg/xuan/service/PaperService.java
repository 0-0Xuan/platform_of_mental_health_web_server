package com.qg.xuan.service;

import java.util.List;

import com.qg.xuan.bean.Grade;
import com.qg.xuan.bean.Paper;
import com.qg.xuan.bean.Situation;
import com.qg.xuan.bean.Worker;

public interface PaperService {
    public int createPaper(Paper paper, String workNum);

    public int writePaper(Paper paper, String studentNum);

    public Grade getGrade(String userId);

    public List<Grade> getGrades(String userId);

    public List<Situation> getSituations(String userId);

    public boolean deletePaper(Paper paper, Worker worker);

    public List<Paper> getPapers(String workNum);

    public Paper getPaper(String paperNum);

    public List<Paper> getStudentPaperGrade(String studentNum);

    public List<Paper> getStudentNotWritePaper(String studentNum);

}

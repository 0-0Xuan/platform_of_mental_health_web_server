package com.qg.xuan.serviceImpl;

import java.util.List;

import com.qg.xuan.bean.GsonBean;
import com.qg.xuan.bean.Student;
import com.qg.xuan.bean.User;
import com.qg.xuan.bean.Worker;
import com.qg.xuan.dao.UserDao;
import com.qg.xuan.service.UserService;
import com.qg.xuan.util.FinalValue;
import com.qg.xuan.util.ShareUtil;

public class UserServiceImpl implements UserService {

    UserDao userDao;

    public void setUserDao(UserDao userDao) {
	this.userDao = userDao;
    }

    @Override
    public String register(User user, String password) {
	// TODO Auto-generated method stub
	if (user != null && user.userId != null && password != null) {
	    System.out.println("user != null && user.userId != null && password != null");
	    GsonBean bean = new GsonBean();
	    if (userDao.insertUser(user, password)) {
		return ShareUtil.gson.toJson(bean);
	    } else {
		bean.state = FinalValue.FAIL;
		return ShareUtil.gson.toJson(bean);
	    }
	}
	System.out.println("返回空");
	return null;
    }

    @Override
    public boolean login(User user, String password) {
	// TODO Auto-generated method stub
	return userDao.hasUser(user, password);
    }

    @Override
    public boolean sureUser(User user) {
	// TODO Auto-generated method stub
	return userDao.sureUser(user, FinalValue.ACTIVE);
    }

    @Override
    public boolean manageUser(User user, String userId) {
	// TODO Auto-generated method stub
	User sureUser = new User();
	sureUser.role = FinalValue.STUDENT;
	sureUser.userId = userId;
	if (userDao.sureUser(sureUser, FinalValue.ACTIVE)) {
	    if (userDao.manageUser(user, userId)) {
		return true;
	    }
	}
	return false;
    }

    @Override
    public boolean fillUserInfo(User user) {
	// TODO Auto-generated method stub
	return userDao.getUser(user);
    }

    @Override
    public boolean refuseUser(User user) {
	// TODO Auto-generated method stub
	return userDao.sureUser(user, FinalValue.REFUSE);
    }

    @Override
    public List<Worker> getNotSureWorker() {
	// TODO Auto-generated method stub
	return userDao.getNotSureWorker();
    }

    @Override
    public List<Student> getManageStudents(String workerId) {
	// TODO Auto-generated method stub
	List<Student> students = userDao.getManageStudents(workerId);
	for (int i = 0; i < students.size(); i++) {
	    userDao.getUser(students.get(i));
	}
	return students;
    }

    @Override
    public boolean manageUsers(User user, List<String> users) {
	// TODO Auto-generated method stub
	for (int i = 0; i < users.size(); i++) {
	    if(!manageUser(user, users.get(i))){
		return false;
	    }
	}
	return true;
    }

}

package com.qg.xuan.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import com.qg.xuan.bean.Grade;
import com.qg.xuan.bean.Paper;
import com.qg.xuan.bean.Question;
import com.qg.xuan.bean.Situation;
import com.qg.xuan.bean.Student;
import com.qg.xuan.bean.Worker;
import com.qg.xuan.dao.PaperDao;
import com.qg.xuan.dao.UserDao;
import com.qg.xuan.service.PaperService;
import com.qg.xuan.util.FinalValue;

public class PaperServiceImpl implements PaperService {

    PaperDao paperDao;
    UserDao userDao;

    public void setPaperDao(PaperDao paperDao) {
	this.paperDao = paperDao;
    }

    public void setUserDao(UserDao userDao) {
	this.userDao = userDao;
    }

    @Override
    public int createPaper(Paper paper, String workNum) {
	// TODO Auto-generated method stub
	int paperId = paperDao.createPaper(paper, workNum);// 插入数据库一条问卷基本信息,并获得问卷编号
	if (paperId >= 0) {// 如果插入成功
	    for (int i = 0; i < paper.questions.size(); i++) {
		Question question = paper.questions.get(i);
		int questionId = paperDao.createQuestion(question,
			String.valueOf(paperId));// 调用插入数据库一条问题的方法,并且获得问题编号
		if (questionId >= 0) {// 如果插入成功
		    for (int k = 0; k < question.options.size(); k++) {
			if (paperDao.createOption(question.options.get(k),
				String.valueOf(questionId)) < 0) {//调用插入一条选项进数据库
			    return FinalValue.FAIL;
			}
		    }
		} else {
		    // 插入失败需记录问卷信息,以后可将之前插入的问题删除
		    return FinalValue.FAIL;
		}
	    }
	} else {
	    return FinalValue.FAIL;
	}
	// 全部插入成功
	return FinalValue.SUCCESS;
    }

    @Override
    public boolean deletePaper(Paper paper, Worker worker) {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public List<Paper> getPapers(String workNum) {
	// TODO Auto-generated method stub
	List<Paper> papers = paperDao.getPapers(workNum);
	for (int i = 0; i < papers.size(); i++) {
	    Paper paper = papers.get(i);
	    paper.questions = paperDao.getQuestion(String
		    .valueOf(paper.paperNum));
	    for (int k = 0; k < paper.questions.size(); k++) {
		paper.questions.get(k).options = paperDao.getOption(String
			.valueOf(paper.questions.get(k).questionNum));
	    }
	}
	return papers;
    }

    @Override
    public List<Paper> getStudentPaperGrade(String studentNum) {
	// TODO Auto-generated method stub

	return paperDao.getStudentPaperGrade(studentNum);
    }

    @Override
    public List<Paper> getStudentNotWritePaper(String studentNum) {
	// TODO Auto-generated method stub
	List<Paper> papers = paperDao.getStudentNotWritePaper(studentNum);//获取学生未填写的问卷
	for (int i = 0; i < papers.size(); i++) {
	    Paper paper = papers.get(i);
	    paper.questions = paperDao.getQuestion(String
		    .valueOf(paper.paperNum));//添加问卷的问题
	    for (int k = 0; k < paper.questions.size(); k++) {
		paper.questions.get(k).options = paperDao.getOption(String
			.valueOf(paper.questions.get(k).questionNum));//添加问题的选项
	    }
	}
	return papers;//返回所有未填写的问卷
    }

    @Override
    public Paper getPaper(String paperNum) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public int writePaper(Paper paper, String studentNum) {
	// TODO Auto-generated method stub
	int result = paperDao.writePaper(paper, studentNum);
	if (result == FinalValue.SUCCESS) {
	    return FinalValue.SUCCESS;
	} else {
	    return FinalValue.FAIL;
	}
    }

    @Override
    public Grade getGrade(String userId) {
	// TODO Auto-generated method stub
	Grade grade = new Grade();
	grade.student = new Student();
	grade.student.userId = userId;
	if (userDao.getUser(grade.student)) {
	    grade.papers = paperDao.getStudentPaperGrade(grade.student.userId);
	    for (int i = 0; i < grade.papers.size(); i++) {
		Paper paper = paperDao.getPaper(String.valueOf(grade.papers
			.get(i).paperNum));
		grade.papers.get(i).instructions = paper.instructions;
	    }
	}
	return grade;
    }

    @Override
    public List<Grade> getGrades(String userId) {
	// TODO Auto-generated method stub
	List<Grade> grades = new ArrayList<Grade>();
	List<Student> students = userDao.getManageStudents(userId);
	for (int i = 0; i < students.size(); i++) {
	    grades.add(getGrade(students.get(i).userId));
	}
	return grades;
    }

    @Override
    public List<Situation> getSituations(String userId) {
	// TODO Auto-generated method stub
	List<Situation> situations = new ArrayList<Situation>();
	List<Grade> grades = getGrades(userId);
	List<Paper> papers = getPapers(userId);

	for (int i = 0; i < papers.size(); i++) {
	    int sum = 0, sumMale = 0, sumFemale = 0;
	    int gradeMax = getPaperGrade(papers.get(i));
	    int good = (int) (gradeMax * 0.8);
	    int middle = (int) (gradeMax * 0.6);
	    Paper iPaper = papers.get(i);
	    Situation s = new Situation();
	    for (int k = 0; k < grades.size(); k++) {
		Grade grade = grades.get(k);
		for (int p = 0; p < grade.papers.size(); p++) {
		    Paper pPaper = grade.papers.get(p);
		    Student student = grade.student;
		    if (pPaper.paperNum == iPaper.paperNum) {// 统计开始
			s.count++;
			sum += pPaper.grade;
			if (pPaper.grade > good) {
			    s.good++;
			} else {
			    if (pPaper.grade > middle) {
				s.middle++;
			    } else {
				s.not_pass++;
			    }
			}

			if (student.sex.equals("m")) {
			    sumMale += pPaper.grade;
			    s.countMale++;
			} else {
			    sumFemale += pPaper.grade;
			    s.countFemale++;
			}
		    }
		}
	    }
	    if (s.count != 0) {
		s.AVG = (int) (sum / s.count + 0.5);
		if (s.countFemale != 0)
		    s.AVGFemale = (int) (sumFemale / s.countFemale + 0.5);
		if (s.countMale != 0)
		    s.AVGMale = (int) (sumMale / s.countMale + 0.5);
	    }

	    situations.add(s);

	}

	return situations;
    }

    public int getPaperGrade(Paper paper) {
	int sum = 0;
	for (int i = 0; i < paper.questions.size(); i++) {
	    sum += getQuestionGrade(paper.questions.get(i));
	}
	return sum;
    }

    public int getQuestionGrade(Question q) {
	int max = 0;
	for (int i = 0; i < q.options.size(); i++) {
	    if (q.options.get(i).grade > max) {
		max = q.options.get(i).grade;
	    }
	}
	return max;
    }

}

package com.qg.xuan.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.CannotGetJdbcConnectionException;

import com.mysql.jdbc.Statement;
import com.qg.xuan.bean.Grade;
import com.qg.xuan.bean.Option;
import com.qg.xuan.bean.Paper;
import com.qg.xuan.bean.Question;
import com.qg.xuan.dao.PaperDao;
import com.qg.xuan.util.FinalValue;
import com.qg.xuan.util.ShareUtil;

public class PaperDaoImpl extends BaseDao implements PaperDao {

    private Map<String, String> map = new HashMap<String, String>();
    private String table;
    private List<String> list = new ArrayList<String>();

    @Override
    public int createPaper(Paper paper, String workNum) {
	// TODO Auto-generated method stub
	table = "paper";
	map.clear();
	map.put("paper_instructions", paper.instructions);
	map.put("work_num", workNum);
	String sql = getInsert(map, table);
	return insertReturnGeneratedKey(sql);

    }

    @Override
    public int createQuestion(Question question, String paperNum) {
	// TODO Auto-generated method stub
	table = "question";
	map.clear();
	map.put("question_content", question.content);
	map.put("paper_num", paperNum);
	String sql = getInsert(map, table);
	return insertReturnGeneratedKey(sql);
    }

    @Override
    public int createOption(Option option, String questionNum) {
	// TODO Auto-generated method stub
	table = "option";
	map.clear();
	map.put("option_content", option.content);
	map.put("question_num", questionNum);
	map.put("option_grade", String.valueOf(option.grade));
	String sql = getInsert(map, table);
	// insertInTable(map, table);
	// return 0;
	return insertReturnGeneratedKey(sql);
    }

    @Override
    public List<Paper> getPapers(String workNum) {
	// TODO Auto-generated method stub
	table = "paper";
	list.clear();
	list.add("paper_num");
	list.add("paper_instructions");
	map.clear();
	map.put("work_num", workNum);
	List<Map<String, Object>> result = selectInTable(list, map, table);
	List<Paper> resultList = new ArrayList<Paper>();
	for (int i = 0; i < result.size(); i++) {
	    Paper paper = new Paper();
	    paper.instructions = result.get(i).get("paper_instructions")
		    .toString();
	    paper.paperNum = Integer.parseInt(result.get(i).get("paper_num")
		    .toString());
	    resultList.add(paper);
	}
	return resultList;
    }

    @Override
    public List<Option> getOption(String questionNum) {
	// TODO Auto-generated method stub
	table = "option";
	list.clear();
	list.add("option_num");
	list.add("option_content");
	list.add("option_grade");
	map.clear();
	map.put("question_num", questionNum);
	List<Map<String, Object>> result = selectInTable(list, map, table);
	List<Option> resultList = new ArrayList<Option>();
	for (int i = 0; i < result.size(); i++) {
	    Option option = new Option();
	    option.content = result.get(i).get("option_content").toString();
	    option.grade = Integer.parseInt(result.get(i).get("option_grade")
		    .toString());
	    option.optionNum = Integer.parseInt(result.get(i).get("option_num")
		    .toString());
	    resultList.add(option);
	}
	return resultList;
    }

    @Override
    public List<Question> getQuestion(String paperNum) {
	// TODO Auto-generated method stub
	table = "question";
	list.clear();
	list.add("question_num");
	list.add("question_content");
	map.clear();
	map.put("paper_num", paperNum);
	List<Map<String, Object>> result = selectInTable(list, map, table);
	List<Question> resultList = new ArrayList<Question>();
	for (int i = 0; i < result.size(); i++) {
	    Question question = new Question();
	    question.content = result.get(i).get("question_content").toString();
	    question.questionNum = Integer.parseInt(result.get(i)
		    .get("question_num").toString());
	    resultList.add(question);
	}
	return resultList;
    }

    @Override
    public List<Paper> getStudentPaperGrade(String studentNum) {
	// TODO Auto-generated method stub
	table = "write_paper";
	list.clear();
	list.add("paper_num");
	list.add("grade");
	map.clear();
	map.put("student_num", studentNum);
	List<Map<String, Object>> result = selectInTable(list, map, table);
	List<Paper> resultList = new ArrayList<Paper>();
	for (int i = 0; i < result.size(); i++) {
	    Paper paper = new Paper();
	    paper.grade = (Integer) (result.get(i).get("grade"));
	    paper.paperNum = Integer.parseInt(result.get(i).get("paper_num")
		    .toString());
	    resultList.add(paper);
	}
	return resultList;
    }

    @Override
    public List<Paper> getStudentNotWritePaper(String studentNum) {
	// TODO Auto-generated method stub
	//获取学生已填写的表的主键后返回
	String sql = "select paper_num,paper_instructions from pmh.paper where(paper_num not in ("
		+ "select paper_num from pmh.write_paper where(student_num = " 
		+studentNum+")))";
	List<Map<String, Object>> result = getJdbcTemplate().queryForList(sql);
	List<Paper> resultList = new ArrayList<Paper>();
	for (int i = 0; i < result.size(); i++) {
	    Paper paper = new Paper();
	    paper.paperNum = Integer.parseInt(result.get(i).get("paper_num")
		    .toString());
	    paper.instructions = result.get(i).get("paper_instructions")
		    .toString();
	    resultList.add(paper);
	}
	System.out.println(sql);
	System.out.println(ShareUtil.gson.toJson(resultList));
	return resultList;
    }

    @Override
    public int writePaper(Paper paper, String studentNum) {
	// TODO Auto-generated method stub
	table = "write_paper";
	map.clear();
	map.put("student_num", studentNum);
	map.put("grade", String.valueOf(paper.grade));
	map.put("paper_num", String.valueOf(paper.paperNum));

	boolean result = insertInTable(map, table);
	if (result) {
	    return FinalValue.SUCCESS;
	} else {
	    return FinalValue.FAIL;
	}
    }

    @Override
    public Paper getPaper(String paperNum) {
	// TODO Auto-generated method stub

	table = "paper";
	list.clear();
	list.add("paper_instructions");
	map.clear();
	map.put("paper_num", paperNum);
	List<Map<String, Object>> result = selectInTable(list, map, table);

	Paper paper = new Paper();
	paper.instructions = result.get(0).get("paper_instructions").toString();
	return paper;
    }

}

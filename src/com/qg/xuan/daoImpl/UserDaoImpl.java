package com.qg.xuan.daoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.qg.xuan.bean.Student;
import com.qg.xuan.bean.User;
import com.qg.xuan.bean.Worker;
import com.qg.xuan.dao.UserDao;
import com.qg.xuan.util.FinalValue;

public class UserDaoImpl extends BaseDao implements UserDao {

    private Map<String, String> map = new HashMap<String, String>();
    private List<String> list = new ArrayList<String>();
    private String table;

    @Override
    public boolean hasUser(User user, String password) {
	// TODO Auto-generated method stub
	map.clear();
	switch (user.role) {
	case FinalValue.MANAGER:
	    map.put("manager_num", user.userId);
	    table = "manager";
	    break;
	case FinalValue.STUDENT:
	    map.put("student_num", user.userId);
	    table = "student";
	    break;
	case FinalValue.WORKER:
	    map.put("work_num", user.userId);
	    table = "work";
	    break;
	}
	map.put("password", password);
	map.put("state", String.valueOf(FinalValue.ACTIVE));

	return hasInTable(map, table);
    }

    @Override
    public boolean insertUser(User user, String password) {
	// TODO Auto-generated method stub
	map.clear();
	switch (user.role) {
	case FinalValue.STUDENT:
	    map.put("student_num", user.userId);
	    table = "student";
	    break;
	case FinalValue.WORKER:
	    map.put("work_num", user.userId);
	    map.put("phone", user.phone);
	    table = "work";
	    break;
	}
	map.put("name", user.name);
	map.put("sex", user.sex);
	map.put("birthday", user.birthday);
	map.put("password", password);
	return insertInTable(map, table);
    }

    @Override
    public List<String> getWorkNum(String studentNum) {
	// TODO Auto-generated method stub
	list.clear();
	list.add("work_num");
	map.clear();
	map.put("student_num", studentNum);
	table = "manage_student";
	List<Map<String, Object>> result = selectInTable(list, map, table);
	List<String> resultList = new ArrayList<String>();
	for (int i = 0; i < result.size(); i++) {
	    resultList.add(result.get(i).get("work_num").toString());
	}
	return resultList;
    }

    @Override
    public boolean sureUser(User user, int state) {
	// TODO Auto-generated method stub
	Map<String, String> where = new HashMap<String, String>();
	switch (user.role) {
	case FinalValue.WORKER:
	    table = "work";
	    where.put("work_num", user.userId);
	    break;
	case FinalValue.STUDENT:
	    table = "student";
	    where.put("student_num", user.userId);
	    break;
	}
	map.clear();
	map.put("state", String.valueOf(state));

	return updateTable(map, where, table);
    }

    @Override
    public boolean manageUser(User user, String userId) {
	// TODO Auto-generated method stub
	map.clear();
	switch (user.role) {
	case FinalValue.WORKER:
	    table = "manage_student";
	    map.put("work_num", user.userId);
	    map.put("student_num", userId);
	    break;
	// case FinalValue.MANAGER:
	// table = "student";
	// break;
	}

	return insertInTable(map, table);
    }

    @Override
    public boolean getUser(User user) {
	// TODO Auto-generated method stub
	list.clear();
	map.clear();
	switch (user.role) {
	case FinalValue.WORKER:
	    table = "work";
	    list.add("phone");
	    map.put("work_num", user.userId);
	    break;
	case FinalValue.STUDENT:
	    table = "student";
	    map.put("student_num", user.userId);
	    break;
	}
	list.add("name");
	list.add("sex");
	list.add("birthday");
	List<Map<String, Object>> result = selectInTable(list, map, table);
	if (result.size() == 1) {
	    user.name = result.get(0).get("name").toString();
	    user.sex = result.get(0).get("sex").toString();
	    user.birthday = result.get(0).get("birthday").toString();
	    Object phone = result.get(0).get("birthday");
	    if (phone != null) {
		user.phone = phone.toString();
	    }
	    return true;
	}
	return false;
    }

    @Override
    public List<Worker> getNotSureWorker() {
	// TODO Auto-generated method stub
	list.clear();
	map.clear();
	table = "work";
	list.add("work_num");
	list.add("name");
	list.add("sex");
	list.add("birthday");
	list.add("phone");
	map.put("state", String.valueOf(FinalValue.DEFAULT));
	List<Map<String, Object>> result = selectInTable(list, map, table);
	List<Worker> workers = new ArrayList<Worker>();
	for (int i = 0; i < result.size(); i++) {
	    Worker worker = new Worker();
	    worker.userId=result.get(i).get("work_num").toString();
	    worker.name=result.get(i).get("name").toString();
	    worker.sex=result.get(i).get("sex").toString();
	    worker.birthday=result.get(i).get("birthday").toString();
	    worker.phone=result.get(i).get("phone").toString();
	    workers.add(worker);
	}
	return workers;
    }

    @Override
    public List<Student> getManageStudents(String workerId) {
	// TODO Auto-generated method stub
	list.clear();
	map.clear();
	table = "manage_student";
	list.add("student_num");
	map.put("work_num", workerId);
	List<Map<String, Object>> result = selectInTable(list, map, table);
	List<Student> students = new ArrayList<Student>();
	for (int i = 0; i < result.size(); i++) {
	    Student student = new Student();
	    student.userId=result.get(i).get("student_num").toString();
	    students.add(student);
	}
	return students;
    }

}

package com.qg.xuan.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.mysql.jdbc.Statement;

public class BaseDao extends JdbcDaoSupport {

    public boolean hasInTable(Map<String, String> map, String table) {
	// TODO Auto-generated method stub
	try {
	    StringBuffer sql = new StringBuffer("select count(*) from ");
	    sql.append('`');
	    sql.append(table);
	    sql.append('`');
	    sql.append(" where(1=1");
	    for (Entry<String, String> entry : map.entrySet()) {
		sql.append(" and ");
		sql.append(entry.getKey());
		sql.append("=");
		sql.append('\'');
		sql.append(entry.getValue());
		sql.append('\'');
	    }
	    sql.append(")");
	    System.out.println(sql);
	    int count = getJdbcTemplate().queryForInt(sql.toString());
	    if (count >= 1) {
		return true;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    public boolean insertInTable(Map<String, String> map, String table) {
	// TODO Auto-generated method stub
	try {
	    StringBuffer sql = new StringBuffer("insert into ");
	    sql.append('`');
	    sql.append(table);
	    sql.append('`');
	    sql.append("(");
	    for (Entry<String, String> entry : map.entrySet()) {
		sql.append(entry.getKey());
		sql.append(",");
	    }
	    sql.setCharAt(sql.length() - 1, ')');
	    sql.append("values(");
	    for (Entry<String, String> entry : map.entrySet()) {
		sql.append('\'');
		sql.append(entry.getValue());
		sql.append('\'');
		sql.append(",");
	    }
	    sql.setCharAt(sql.length() - 1, ')');
	    System.out.println(sql);
	    getJdbcTemplate().update(sql.toString());
	    return true;
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    public boolean deleteFromTable(Map<String, String> map, String table) {
	// TODO Auto-generated method stub
	try {
	    StringBuffer sql = new StringBuffer("delete from ");
	    sql.append('`');
	    sql.append(table);
	    sql.append('`');
	    sql.append(" where(1=1");
	    for (Entry<String, String> entry : map.entrySet()) {
		sql.append(" and ");
		sql.append(entry.getKey());
		sql.append("=");
		sql.append('\'');
		sql.append(entry.getValue());
		sql.append('\'');
	    }
	    sql.append(")");
	    System.out.println(sql);
	    getJdbcTemplate().update(sql.toString());
	    return true;
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    public boolean updateTable(Map<String, String> set,
	    Map<String, String> where, String table) {
	// TODO Auto-generated method stub
	try {
	    StringBuffer sql = new StringBuffer("update ");
	    sql.append('`');
	    sql.append(table);
	    sql.append('`');
	    sql.append(" set ");
	    for (Entry<String, String> entry : set.entrySet()) {
		sql.append('`');
		sql.append(entry.getKey());
		sql.append('`');
		sql.append("=");
		sql.append('\'');
		sql.append(entry.getValue());
		sql.append('\'');
		sql.append(",");
	    }
	    sql.setCharAt(sql.length() - 1, ' ');
	    sql.append(" where(1=1");
	    for (Entry<String, String> entry : where.entrySet()) {
		sql.append(" and ");
		sql.append(entry.getKey());
		sql.append("=");
		sql.append('\'');
		sql.append(entry.getValue());
		sql.append('\'');
	    }
	    sql.append(")");
	    System.out.println(sql);
	    getJdbcTemplate().update(sql.toString());
	    return true;
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    public List<Map<String,Object>> selectInTable(List<String> select,
	    Map<String, String> map, String table) {
	StringBuffer sql=new StringBuffer("select ");
	for(int i=0;i<select.size();i++){
	    sql.append("`");
	    sql.append(select.get(i));
	    sql.append("`");
	    sql.append(",");
	}
	sql.setCharAt(sql.length() - 1, ' ');
	sql.append("from ");
	sql.append("`");
	sql.append(table);
	sql.append("`");
	sql.append(" where(");
	for(Entry<String, String> entry : map.entrySet()){
	    sql.append("`");
	    sql.append(entry.getKey());
	    sql.append("`=");
	    sql.append("'");
	    sql.append(entry.getValue());
	    sql.append("',");
	}
	sql.setCharAt(sql.length() - 1, ')');
	System.out.println(sql);
	return getJdbcTemplate().queryForList(sql.toString());
    }

    public int insertReturnGeneratedKey(final String sql) {

	System.out.println(sql);
	// 创建一个主键持有者
	Statement stmt;
	int key = -1;
	try {
	    stmt = (Statement) getConnection().createStatement();
	    stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
	    ResultSet rs = stmt.getGeneratedKeys(); // 得到新插入记录的自增主键
	    if (rs.next()) {
		key = rs.getInt(1);
	    }
	    return key;
	} catch (CannotGetJdbcConnectionException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return -1;

    }

    public String getInsert(Map<String, String> map, String table) {
	StringBuffer sql = new StringBuffer("insert into ");
	sql.append('`');
	sql.append(table);
	sql.append('`');
	sql.append("(");
	for (Entry<String, String> entry : map.entrySet()) {
	    sql.append('`');
	    sql.append(entry.getKey());
	    sql.append('`');
	    sql.append(",");
	}
	sql.setCharAt(sql.length() - 1, ')');
	sql.append("values(");
	for (Entry<String, String> entry : map.entrySet()) {
	    sql.append('\'');
	    sql.append(entry.getValue());
	    sql.append('\'');
	    sql.append(",");
	}
	sql.setCharAt(sql.length() - 1, ')');
	System.out.println(sql);
	return sql.toString();
    }

}
